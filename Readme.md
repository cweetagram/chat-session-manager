# Cweetagram Chat Session manager

This package makes it easy to manage the flow for your chat screens.
This package is used to manage USSD, Whatsapp or SMS conversations.

## Installation

    $ composer require cweetagram/chat-session-manager 


## Getting started

As you execute the following file, the steps will keep on increasing as set per condition.
The default process is 1 and default step is 1.

    require_once __DIR__ . '/vendor/autoload.php';
    
    use CweetgramSolutions\Helper\ChatSession;
    //An example of a unique session identifier
    $session = '77e6e243c61670f632dfe1149d5ec547';
    
    ChatSession::findLastStep($session);
    
    //Here we handle first step of the chat
    if ((ChatSession::$process == 1) && (ChatSession::$step == 1)) {
        // Here we set the next step the condition should execute
        ChatSession::nextStep($session, 1, 2);
        echo "Welcome to step one".PHP_EOL;
        die();
    }
    
    //Here we handle second step of the chat
    if ((ChatSession::$process == 1) && (ChatSession::$step == 2)) {
        // Here we set the next step the condition should execute
        ChatSession::nextStep($session, 1, 3);
        echo "Welcome to step two".PHP_EOL;
        die();
    }
    
    //Here we handle third step of the chat
    if ((ChatSession::$process == 1) && (ChatSession::$step == 3)) {
        // Here we set the next step the condition should execute
        ChatSession::nextStep($session, 1, 4);
        echo "Welcome to step three".PHP_EOL;
        die();
    }
    
    //Here we handle third and last step of the chat
    if ((ChatSession::$process == 1) && (ChatSession::$step == 4)) {
        // Here we set the next step the condition should execute
        ChatSession::clearSteps($session);
        echo "Session cleared you will start over from now on".PHP_EOL;
        die();
    }

# Tests

    $ ./vendor/bin/phpunit tests

# Credits

This is a property of [Cweetagram Solutions](https://cweetagramsolutions.com). For any queries contact:
[Chancel Shongo - CTO](mailto:chancel@cweetagram.co.za)
or
[Dev team](mailto:dev@cweetagram.co.za)
