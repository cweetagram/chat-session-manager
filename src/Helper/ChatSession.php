<?php

namespace CweetgramSolutions\Helper;

use CweetgramSolutions\SessionFileManager;

class ChatSession
{
    /**
     * @var
     */
    public static $process;

    /**
     * @var
     */
    public static $step;

    /**
     * Function returns current process and step for a specific session identifier
     *
     * @param $ui_id
     * @return void
     */
    public static function findLastStep($ui_id)
    {
        $sessionManager = new SessionFileManager($ui_id);
        self::$process = $sessionManager->process;
        self::$step = $sessionManager->step;
    }

    /**
     * Function updates step and/or process of a specific session identifier
     *
     * @param $ui_id
     * @param int $process
     * @param int $step
     * @return void
     */
    public static function nextStep($ui_id, $process, $step)
    {
        $sessionManager = new SessionFileManager($ui_id);
        $sessionManager->setSessionStep($process, $step);
        self::$process = $sessionManager->process;
        self::$step = $sessionManager->step;
    }

    /**
     * Function clears step and/or process of a specific session identifier
     * @param $ui_id
     * @return void
     */
    public static function clearSteps($ui_id)
    {
        $sessionManager = new SessionFileManager($ui_id);
        $sessionManager->finishSession();
        self::$process = 0;
        self::$step = 0;
    }
}
