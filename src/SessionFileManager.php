<?php

namespace CweetgramSolutions;

class SessionFileManager
{
    /**
     * @var
     */
    protected $uu_id;

    /**
     * @var int
     */
    public $process = 1;

    /**
     * @var int
     */
    public $step = 1;

    /**
     * SessionFileManager constructor.
     * @param $uu_id
     */
    public function __construct($uu_id)
    {
        $this->uu_id = $uu_id;
        $this->getSessionLastStep();
    }

    /**
     * @param $process
     * @param $step
     * @return void
     */
    public function setSessionStep($process = 1, $step = 1)
    {
        if (! file_exists(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json')) {
            $file = fopen(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json', "w");
            fwrite($file, json_encode(['process' => $process, 'step' => $step]));
            fclose($file);
            $this->process = $process;
            $this->step = $step;
        } else {
            $file = fopen(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json', "w");
            fwrite($file, json_encode(['process' => $process, 'step' => $step]));
            fclose($file);
            $this->process = $process;
            $this->step = $step;
        }
    }

    /**
     * @return void
     */
    public function finishSession()
    {
        if (file_exists(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json')) {
            unlink(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json');
        }
    }

    /**
     * @return void
     */
    public function getSessionLastStep()
    {
        if (file_exists(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json')) {
            $json = file_get_contents(dirname(__FILE__).'/sessions/'.$this->uu_id.'.json');
            $step_data = json_decode($json);
            $this->process = $step_data->process;
            $this->step = $step_data->step;
        } else {
            $this->setSessionStep();
        }
    }
}
