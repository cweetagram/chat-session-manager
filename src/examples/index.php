<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use CweetgramSolutions\Helper\ChatSession;

$session = '77e6e243c61670f632dfe1149d5ec547';

echo 'Generated session id: '.$session.PHP_EOL;

ChatSession::findLastStep($session);

//Here we handle first step of the chat
if ((ChatSession::$process == 1) && (ChatSession::$step == 1)) {
    // Here we set the next step the condition should execute
    ChatSession::nextStep($session, 1, 2);
    echo "Welcome to step one".PHP_EOL;
    die();
}

//Here we handle second step of the chat
if ((ChatSession::$process == 1) && (ChatSession::$step == 2)) {
    // Here we set the next step the condition should execute
    ChatSession::nextStep($session, 1, 3);
    echo "Welcome to step two".PHP_EOL;
    die();
}

//Here we handle third step of the chat
if ((ChatSession::$process == 1) && (ChatSession::$step == 3)) {
    // Here we set the next step the condition should execute
    ChatSession::nextStep($session, 1, 4);
    echo "Welcome to step three".PHP_EOL;
    die();
}

//Here we handle third and last step of the chat
if ((ChatSession::$process == 1) && (ChatSession::$step == 4)) {
    // Here we set the next step the condition should execute
    ChatSession::clearSteps($session);
    echo "Session cleared you will start over from now on".PHP_EOL;
    die();
}
