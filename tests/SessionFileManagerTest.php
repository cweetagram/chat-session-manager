<?php

use PHPUnit\Framework\TestCase;
use CweetgramSolutions\SessionFileManager;

class SessionFileManagerTest extends TestCase
{
    public function testSessionFile()
    {
        $uiid = rand(00000000, 99999999999);
        $fileManager = new SessionFileManager($uiid);
        $this->assertFileExists(dirname(__FILE__).'/../src/sessions/'.$uiid.'.json');
        $this->assertEquals(1, $fileManager->process);
        $this->assertEquals(1, $fileManager->step);
        $fileManager->setSessionStep(1, 2);
        $this->assertEquals(1, $fileManager->process);
        $this->assertEquals(2, $fileManager->step);
        $fileManager->setSessionStep(3, 1);
        $this->assertEquals(3, $fileManager->process);
        $this->assertEquals(1, $fileManager->step);
        $fileManager->finishSession();
        $this->assertFileNotExists(dirname(__FILE__).'/../src/sessions/'.$uiid.'.json');
        $fileManager->finishSession();
    }

    public function testChatSessionHelper()
    {
        $uiid = rand(00000000, 99999999999);
        \CweetgramSolutions\Helper\ChatSession::findLastStep($uiid);
        $this->assertEquals(1, \CweetgramSolutions\Helper\ChatSession::$step);
        $this->assertEquals(1, \CweetgramSolutions\Helper\ChatSession::$process);
        \CweetgramSolutions\Helper\ChatSession::nextStep($uiid, 1, 2);
        $this->assertEquals(2, \CweetgramSolutions\Helper\ChatSession::$step);
        $this->assertEquals(1, \CweetgramSolutions\Helper\ChatSession::$process);
        \CweetgramSolutions\Helper\ChatSession::clearSteps($uiid);
        $this->assertEquals(0, \CweetgramSolutions\Helper\ChatSession::$step);
        $this->assertEquals(0, \CweetgramSolutions\Helper\ChatSession::$process);
    }
}
